package vn.vnpay.taskdemo.service;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import vn.vnpay.taskdemo.DTO.MerchantCreateRequest;
import vn.vnpay.taskdemo.entity.Merchant;
import vn.vnpay.taskdemo.exception.MerchantCodeExistsException;
import vn.vnpay.taskdemo.exception.MerchantCodeLengthException;
import vn.vnpay.taskdemo.exception.MerchantNameLengthException;
import vn.vnpay.taskdemo.exception.TokenExistsException;
import vn.vnpay.taskdemo.repository.MerchantRepository;

import java.util.List;

@Slf4j
@Service
public class MerchantServiceImpl implements MerchantService {

    private final MerchantRepository merchantRepository;
    private final ModelMapper modelMapper;
    private static final int MERCHANT_CODE_LENGTH_MAX = 10;
    private static final int MERCHANT_CODE_LENGTH_MIN = 5;
    private static final int MERCHANT_NAME_LENGTH_MAX = 15;
    private static final int MERCHANT_NAME_LENGTH_MIN = 10;


    public MerchantServiceImpl(MerchantRepository merchantRepository, ModelMapper modelMapper) {
        this.merchantRepository = merchantRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    @Synchronized
    public Merchant createMerchant(MerchantCreateRequest merchantCreateRequest) {

        log.info("start crate merchant");

        List<Merchant> merchantsHaveTokenExists = merchantRepository.findByToken(merchantCreateRequest.getToken());
        int lengthOfMerchantCode = merchantCreateRequest.getMerchantCode().length();
        int lengthOfMerchantName = merchantCreateRequest.getMerchantName().length();

        if (merchantRepository.existsByMerchantCode(merchantCreateRequest.getMerchantCode())) {
            throw new MerchantCodeExistsException();
        }
        if (lengthOfMerchantCode > MERCHANT_CODE_LENGTH_MAX || lengthOfMerchantCode < MERCHANT_CODE_LENGTH_MIN) {
            throw new MerchantCodeLengthException();
        }
        if (merchantsHaveTokenExists != null) {
            for (int i = 0; i < merchantsHaveTokenExists.size(); i++) {
                if (merchantsHaveTokenExists.get(i).getToken().equals(merchantCreateRequest.getToken())
                        && merchantCreateRequest.getCreateDate().getDay() == merchantsHaveTokenExists.get(i).getCreateDate().getDay()
                        && merchantCreateRequest.getCreateDate().getMonth() == merchantsHaveTokenExists.get(i).getCreateDate().getMonth()
                        && merchantCreateRequest.getCreateDate().getYear() == merchantsHaveTokenExists.get(i).getCreateDate().getYear()) {
                    throw new TokenExistsException();
                }
            }
        }
        if (lengthOfMerchantName > MERCHANT_NAME_LENGTH_MAX || lengthOfMerchantName < MERCHANT_NAME_LENGTH_MIN) {
            throw new MerchantNameLengthException();
        }
        Merchant merchant = modelMapper.map(merchantCreateRequest, Merchant.class);
        log.info("return entity");
        return merchantRepository.save(merchant);
    }
}
