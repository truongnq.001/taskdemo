package vn.vnpay.taskdemo.service;

import vn.vnpay.taskdemo.DTO.MerchantCreateRequest;
import vn.vnpay.taskdemo.entity.Merchant;

public interface MerchantService {
    Merchant createMerchant(MerchantCreateRequest merchantCreateRequest);
}
