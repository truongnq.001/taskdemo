package vn.vnpay.taskdemo.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;

import javax.validation.constraints.Max;
import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.Date;

@Entity
@Table(name = "Merchant")
@Data
@SuperBuilder
@NoArgsConstructor
public class Merchant {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "token", nullable = false)
    private String token;

    @Column(name = "merchant_code", nullable = false)
    private String merchantCode;

    @Column(name = "merchant_name", nullable = false)
    private String merchantName;

    @Temporal(TemporalType.DATE)
    @Column(name = "create_date")
    private Date createDate;
}
