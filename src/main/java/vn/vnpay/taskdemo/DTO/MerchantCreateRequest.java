package vn.vnpay.taskdemo.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class MerchantCreateRequest {
    @NotNull
    private String token;

    @NotNull
    private String merchantCode;

    @NotNull
    private String merchantName;

    private Date createDate = new Date();
}
