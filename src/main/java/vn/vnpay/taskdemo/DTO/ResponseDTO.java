package vn.vnpay.taskdemo.DTO;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@SuperBuilder
public class ResponseDTO {
    int code;
    String message;
    Long data;
}
