package vn.vnpay.taskdemo.service;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vn.vnpay.taskdemo.DTO.MerchantCreateRequest;
import vn.vnpay.taskdemo.entity.Merchant;
import vn.vnpay.taskdemo.exception.MerchantCodeExistsException;
import vn.vnpay.taskdemo.exception.MerchantCodeLengthException;
import vn.vnpay.taskdemo.exception.MerchantNameLengthException;
import vn.vnpay.taskdemo.exception.TokenExistsException;
import vn.vnpay.taskdemo.repository.MerchantRepository;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@Slf4j
@ExtendWith(MockitoExtension.class)
class MerchantServiceImplTest {

    @Mock
    private MerchantRepository merchantRepository;

    private MerchantService merchantService;

    @BeforeEach
    public void setUp() {
        merchantService = new MerchantServiceImpl(
                merchantRepository,
                new ModelMapper()
        );
    }

    @Test
    void createMerchantSuccess() {
        log.info("start test createMerchantSuccess");
        Merchant mockMerchantInDB = Merchant.builder()
                .id(1L)
                .token("00000000001")
                .merchantCode("00001")
                .merchantName("Merchant A")
                .build();


        when(merchantRepository.existsByMerchantCode(anyString())).thenReturn(false);

        when(merchantRepository.findByToken(anyString())).thenReturn(null);

        when(merchantRepository.save(any(Merchant.class))).thenReturn(mockMerchantInDB);

        MerchantCreateRequest inputMerchant = MerchantCreateRequest.builder()
                .token("00000000001")
                .merchantCode("00001")
                .merchantName("Merchant A")
                .build();

        Merchant merchantExpected = Merchant.builder()
                .id(1L)
                .token("00000000001")
                .merchantCode("00001")
                .merchantName("Merchant A")
                .build();

        Merchant merchantActual = merchantService.createMerchant(inputMerchant);

        assertNotNull(merchantActual);
        assertEquals(merchantExpected, merchantActual);

        log.info("return");
    }

    @SneakyThrows
    @Test
    void createMerchantSuccessWhenTokenExists() {
        log.info("start test createMerchantSuccessWhenTokenExists");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Merchant mockMerchantInDB = Merchant.builder()
                .id(1L)
                .token("00000000000")
                .merchantCode("00001")
                .merchantName("Merchant A")
                .createDate(simpleDateFormat.parse("10-7-2021"))
                .build();

        Merchant mockMerchantSave = Merchant.builder()
                .id(1L)
                .token("00000000000")
                .merchantCode("00001")
                .merchantName("Merchant A")
                .createDate(simpleDateFormat.parse("20-7-2021"))
                .build();

        when(merchantRepository.existsByMerchantCode(anyString())).thenReturn(false);

        when(merchantRepository.findByToken(anyString())).thenReturn(Collections.singletonList(mockMerchantInDB));

        when(merchantRepository.save(any(Merchant.class))).thenReturn(mockMerchantSave);

        MerchantCreateRequest inputMerchant = MerchantCreateRequest.builder()
                .token("00000000000")
                .merchantCode("00001")
                .merchantName("Merchant A")
                .createDate(simpleDateFormat.parse("20-7-2021"))
                .build();

        Merchant merchantExpected = Merchant.builder()
                .id(1L)
                .token("00000000000")
                .merchantCode("00001")
                .merchantName("Merchant A")
                .createDate(simpleDateFormat.parse("20-7-2021"))
                .build();

        Merchant merchantActual = merchantService.createMerchant(inputMerchant);

        assertNotNull(merchantActual);
        assertEquals(merchantExpected, merchantActual);
        log.info("return");
    }

    @Test
    void createMerchantWhenMerchantCodeExists() {
        log.info("start test createMerchantWhenMerchantCodeExists");
        when(merchantRepository.existsByMerchantCode(anyString())).thenReturn(true);

        MerchantCreateRequest inputMerchant = MerchantCreateRequest.builder()
                .token("00000000001")
                .merchantCode("00001")
                .merchantName("Merchant B")
                .build();
        assertThrows(MerchantCodeExistsException.class, () -> merchantService.createMerchant(inputMerchant));
        log.info("return");
    }

    @Test
    void merchantCodeLengthThanTenCharacter() {
        log.info("start test merchantCodeLengthThanTenCharacter");
        MerchantCreateRequest inputMerchant = MerchantCreateRequest.builder()
                .token("00000000000")
                .merchantCode("00000000000000000001")
                .merchantName("Merchant B")
                .build();

        assertThrows(MerchantCodeLengthException.class, () -> merchantService.createMerchant(inputMerchant));
        log.info("return");
    }

    @Test
    void merchantCodeLengthLessFiveCharacter() {
        log.info("start test merchantCodeLengthLessFiveCharacter");
        MerchantCreateRequest inputMerchant = MerchantCreateRequest.builder()
                .token("00000000000")
                .merchantCode("1")
                .merchantName("Merchant B")
                .build();

        assertThrows(MerchantCodeLengthException.class, () -> merchantService.createMerchant(inputMerchant));
        log.info("return");
    }

    @Test
    void createMerchantWhenTokenExists() {
        log.info("start test createMerchantWhenTokenExists");
        Merchant mockMerchantInDB = Merchant.builder()
                .id(1L)
                .token("00000000000")
                .merchantCode("00001")
                .merchantName("Merchant A")
                .createDate(new Date(2021 - 7 - 16))
                .build();

        when(merchantRepository.findByToken(anyString())).thenReturn(Collections.singletonList(mockMerchantInDB));

        MerchantCreateRequest inputMerchant = MerchantCreateRequest.builder()
                .token("00000000000")
                .merchantCode("00002")
                .merchantName("Merchant B")
                .createDate(new Date(2021 - 7 - 16))
                .build();
        assertThrows(TokenExistsException.class, () -> merchantService.createMerchant(inputMerchant));
        log.info("return");
    }

    @Test
    void merchantNameLengthThanFifteenCharacter() {
        log.info("start test merchantNameLengthThanFifteenCharacter");
        MerchantCreateRequest inputMerchant = MerchantCreateRequest.builder()
                .token("00000000000")
                .merchantCode("00001")
                .merchantName("MerchanttttttttttttttttB")
                .build();
        assertThrows(MerchantNameLengthException.class, () -> merchantService.createMerchant(inputMerchant));
        log.info("return");
    }

    @Test
    void merchantNameLengthLessTenCharacter() {
        log.info("start test merchantNameLengthLessTenCharacter");
        MerchantCreateRequest inputMerchant = MerchantCreateRequest.builder()
                .token("00000000000")
                .merchantCode("00001")
                .merchantName("B")
                .build();
        assertThrows(MerchantNameLengthException.class, () -> merchantService.createMerchant(inputMerchant));
        log.info("return");
    }
}